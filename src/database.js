//require module mysql
const mysql = require('mysql');
//creazione della connessione
const connection = mysql.createPool({
    host:'localhost',
    user:'root',
    password:'root',
    database:'CED_TABLES'
});

//function che ritorna la connection al db
module.exports = {
    getConnection : function(){
        console.log('Connessione al db in corso...');
        //connection.connect();
        return connection;
    },
    closeConnection : function() {
        console.log('Chiusura connessione al db in corso...');
        return connection.end();
    }
}
