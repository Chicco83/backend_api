const express = require('express');
const bodyParser = require('body-parser');
const cors = require('cors');
const helmet = require('helmet');
const morgan = require('morgan');
const query = require('./query');
const JSON = require('circular-json');



const app = express();

app.use(helmet());
app.use(bodyParser.json());
app.use(cors());
app.use(morgan('combined'));


app.get('/getCedolini', (req, res) => {
   query.getCedolini(function(result){
      res.send(JSON.stringify(result));
   });
});
app.get('/getCedolinoById/:id', (req, res) => {
   const id = req.params.id;
   console.log('ID RICHIAMATO',id);
   query.getCedolinoById(id,function(result){
      res.send(JSON.stringify(result));
   });
});
app.post('/saveCedolino', (req, res) => {
   const ced = req.body;
   query.saveCedolini(ced,function(result){
      if(result.error !== undefined){
         res.status(500).send(result);
      }else{
         res.send(JSON.stringify(result));
      }
   });
});
app.post('/updateCedolino', (req, res) => {
   const ced = req.body;
   console.log('Post richiamata ',ced);
   query.updateCedolini(ced,function(result){
      if(result.error !== undefined){
         res.status(500).send(result);
      }else{
         res.send(JSON.stringify(result));
      }
   });
});
app.listen(3001,()=>{
   console.log("Server started on port 3001! daje!");
});