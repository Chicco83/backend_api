const connection = require('./database');

const nomeDb='CED_TABLES';

module.exports = {
    getCedolini : function(callback){
        const conn = connection.getConnection();
        let risultato = {};
        conn.getConnection((err,connection)=>{
            if(err){
                connection.release();
                console.log('Errore durante la connessione',err);
                throw err;
            }
            conn.query('SELECT * FROM TB_CED_CEDOLINI as ListaCed WHERE CED_FLAG_STATO = "A" ',function (err2,rows,fields) {
                if(err2) {
                    console.log('Errore durante la chiamata a db');
                    risultato = {error:true};
                }else{
                    if(rows[0] !== undefined){
                        risultato = rows[0];
                        console.log('Risultato db ',rows[0]);
                    }
                }
                connection.release();
                return callback(risultato);
            });
        });
    },
    getCedolinoById : function(id,callback) {
        const conn = connection.getConnection();
        let risultato = {};
        conn.getConnection((err,connection)=>{
            if(err){
                connection.release();
                console.log('Errore durante la connessione',err);
                throw err;
            }
            const query = `SELECT * FROM TB_CED_CEDOLINI as ListaCed WHERE CED_ID = '${id}'`;
                conn.query(query,function (err2,rows,fields) {``
                    if(err2) throw err2;
                    if(rows[0] !== undefined){
                        risultato = rows[0];
                        console.log('Risultato db ',rows[0]);
                    }
                    connection.release();
                    return callback(risultato);
            });
        });
        
    },
    updateCedolini : function(ced,callback){
        const conn = connection.getConnection();
        let risultato = {};
        conn.getConnection((err,connection)=>{
            if(err){
                connection.release();
                console.log('Errore durante la connessione',err);
                throw err;
            }
            const query = `UPDATE TB_CED_CEDOLINI SET CED_NOMEFILE = '${ced.nomefile}',
                CED_FILESIZE = '${ced.filesize}',
                CED_ANNO = '${ced.anno}',
                CED_MESE = '${ced.mese}',
                CED_DATA_INS = '${ced.datains}',
                CED_DATA_AGG = '${ced.dataagg}',
                CED_UTENTE = '${ced.utente}', CED_FLAG_STATO = '${ced.stato}' WHERE CED_ID = '${ced.id}'`;
                conn.query(query, function (err2,result){
                    if(err2) {
                        console.log('Errore durante la chiamata a db');
                        risultato = {
                            error:true,
                            msg:err2
                        };
                    }else{
                        console.log(result.affectedRows + " record(s) updated");
                        connection.release();
                        risultato = {affected:result.affectedRows};
                    }
                    return callback(risultato);
                });
        });
        
    },
    saveCedolini : function(ced,callback) {
        console.log('Post richiamata ',ced);
        const conn = connection.getConnection();
        let risultato = {};
        conn.getConnection((err,connection)=>{
            if(err){
                connection.release();
                console.log('Errore durante la connessione',err);
                throw err;
            }
            const query = `INSERT INTO TB_CED_CEDOLINI (CED_NOMEFILE,
                CED_FILESIZE,
                CED_ANNO,
                CED_MESE,
                CED_DATA_INS,
                CED_DATA_AGG,
                CED_UTENTE,
                CED_FILE,
                CED_FLAG_STATO
                ) VALUES ('${ced.nomefile}',
                         '${ced.filesize}',
                         '${ced.anno}',
                         '${ced.mese}',
                         '${ced.datains}',
                         '${ced.dataagg}',
                         '${ced.utente}',
                         '${ced.file}',
                         'A')`;
            conn.query(query, function (err2, result) {
                if (err2){
                    console.log('Errore durante la chiamata a db');
                    risultato = {
                        error:true,
                        msg: err2
                    };
                }else{
                    console.log("1 record inserted");
                    risultato = {inserted:true};
                }
                connection.release();
                return callback(risultato);
            });
        });
    }
}